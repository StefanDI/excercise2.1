package com.isoft.internship.excersice2.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;

@Controller
public class HuffmanEncodingController {

    private static final char NON_LETTER_NODE = '*';

    private String encodedString;

    private Map<Character, String> letterEncodingMap;

    @PostMapping(value = "/get-encoding", produces = "application/json")
    public @ResponseBody String getEncoding(@RequestParam("inputText") String inputText) {
//        String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

        letterEncodingMap = new HashMap<>();

        Map<Character, Integer> letterFreqMap = getLetterFrequencyMap(inputText);

        Node rootNode = generateHuffmanTree(letterFreqMap);

        getLetterEncodingMap(rootNode, "");

        encodedString = "";
        inputText.chars()
                .forEach((ch) ->
                        encodedString += letterEncodingMap.get((char)ch));

        int encodedStringLength = encodedString.length();

        int treeHeight = getTreeHeight(rootNode);

        return "{\n" +
                "\"originalString\":\"" + inputText + "\",\n" +
                "\"encodedString\" : \"" + encodedString + "\", \n" +
                "\"encodedStringLength\" : " + encodedStringLength + ", \n" +
                "\"treeHeight\" : " + treeHeight + "\n" +
                "}";
    }

    private Node generateHuffmanTree(Map<Character,Integer> letterFrequencyMap) {

        Node root = null;
        PriorityQueue<Node> queue = new PriorityQueue<>();

        //initialize queue
        letterFrequencyMap.
                forEach((key, value) ->
                        queue.add(new Node(key, value)));

        //Build the huffman tree
        while (queue.size() > 1) {
            Node leftChild = queue.poll();
            Node rightChild = queue.poll();

            Node sumNode = new Node(NON_LETTER_NODE,
                    leftChild.count + rightChild.count);

            sumNode.left = leftChild;
            sumNode.right = rightChild;
            root = sumNode;


            queue.add(sumNode);
        }

        return root;
    }

    private Map<Character, Integer> getLetterFrequencyMap(String text) {
        Map<Character, Integer> letterFreqMap = new HashMap<>();

        text.chars()
                .forEach((letter) ->
                        letterFreqMap.merge((char)letter, 1 ,Integer::sum));

        return letterFreqMap;
    }

    private void getLetterEncodingMap(Node currentNode, String currentNodeBits) {

        if(currentNode.left == null
                && currentNode.right == null
                && currentNode.value != NON_LETTER_NODE) {

            letterEncodingMap.put(currentNode.value, currentNodeBits);
            return;
        }

        getLetterEncodingMap(currentNode.left,  currentNodeBits + "0");
        getLetterEncodingMap(currentNode.right, currentNodeBits + "1");

    }

    private int getTreeHeight(Node root) {
        if(root == null)
            return 0;

        int leftSubTreeHeight = getTreeHeight(root.left);
        int rightSubTreeHeight = getTreeHeight(root.right);

        return Math.max(leftSubTreeHeight, rightSubTreeHeight) + 1;
    }


    private class Node implements Comparable<Node> {

        private int count;
        private char value;

        private Node left;
        private Node right;

        Node(char value, int count) {
            this.count = count;
            this.value = value;
        }

        @Override
        public String toString() {
            return value + " : " + count;
        }

        @Override
        public int compareTo(Node node) {
            return count - node.count;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return count == node.count &&
                    value == node.value;
        }

        @Override
        public int hashCode() {
            return Objects.hash(count, value);
        }
    }}
